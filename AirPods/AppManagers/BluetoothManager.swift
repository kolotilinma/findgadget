//
//  BluetoothManager.swift
//  AirPods
//
//  Created by Михаил Колотилин on 28.03.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit
import CoreBluetooth

class BluetoothManager: NSObject, CBCentralManagerDelegate {
    
    let manager = CBCentralManager()
    var foundDevices = [String]()
    
    override init() {
        super.init()
        manager.delegate = self
        manager.scanForPeripherals(withServices: nil, options: nil)
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        // updates when bluetooth state changes
        manager.scanForPeripherals(withServices: nil, options: [CBCentralManagerScanOptionAllowDuplicatesKey : true])
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        if let name = peripheral.name {
            if foundDevices.contains(name) == false {
                foundDevices.append(name)
                NotificationCenter.default.post(name: NSNotification.Name("didUpdate"), object: nil)
            }
            
            let max = 90.0
            let min = 40.0
            let normalRSSI = (abs(RSSI.doubleValue) - min) / (max - min)
            let signal = CGFloat(1 - normalRSSI * 1.0 ) * 100.0
            let formattedSignal = signal > 100.0 ? 100.0 : (signal < 0.0 ? 0.0 : signal)
            
            let signalData = [name : formattedSignal]
            NotificationCenter.default.post(name: NSNotification.Name("signal"), object: signalData)
        }
    }
}
