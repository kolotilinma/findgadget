//
//  OPLGaugeLayer.swift
//  AirPods
//
//  Created by Михаил Колотилин on 06.04.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit

open class OPLGaugeLayer: CAShapeLayer {
    
    internal var disableSpringAnimation: Bool = true
    
    open override func action(forKey event: String) -> CAAction? {
        if event == "transform" && !disableSpringAnimation {
            let springAnimation = CASpringAnimation(keyPath: event)
            springAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
            springAnimation.initialVelocity = 0.8
            springAnimation.damping = 2
            return springAnimation
        }
        return super.action(forKey: event)
    }
    
}
