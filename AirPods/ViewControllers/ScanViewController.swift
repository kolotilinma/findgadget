//
//  ScanViewController.swift
//  AirPods
//
//  Created by Михаил Колотилин on 28.03.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit

class ScanViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // First Layer
        animateLayer(layer: generateAnimationLayer())
        // Second Layer
        animateLayer(layer: generateAnimationLayer(), delay: 0.3)
        // Animate and show devices screen
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if let window = (UIApplication.shared.delegate as? AppDelegate)?.window {
                window.rootViewController = self.storyboard?.instantiateViewController(identifier: "device")
            }
        }
    }
    
    private func animateLayer(layer: UIView, delay: TimeInterval = 0.0) {
        UIView.animate(withDuration: 1.5, delay: delay, animations: {
            layer.transform = CGAffineTransform(scaleX: 100, y: 100)
            layer.center = self.view.center
            layer.alpha = 0.1
        }) { (_) in
            layer.transform = CGAffineTransform.identity
            layer.center = self.view.center
            layer.alpha = 1.0
            self.animateLayer(layer: layer, delay: delay)
        }
    }
    
    private func generateAnimationLayer() -> UIView {
        let animationLayer = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        animationLayer.backgroundColor = .link
        animationLayer.center = view.center
        animationLayer.layer.cornerRadius = animationLayer.frame.width/2
        view.addSubview(animationLayer)
        view.sendSubviewToBack(animationLayer)
        return animationLayer
    }
    
    
}
