//
//  DevicesViewController.swift
//  AirPods
//
//  Created by Михаил Колотилин on 28.03.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit

class DevicesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tabelView: UITableView!
    let manager = BluetoothManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(forName: NSNotification.Name("didUpdate"), object: nil, queue: nil) { (_) in
            self.tabelView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return manager.foundDevices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? DeviceTableViewCell {
            cell.configure(name: manager.foundDevices[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let devVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "dev") as? DevViewController
        devVC?.name = manager.foundDevices[indexPath.row]
        navigationController?.pushViewController(devVC!, animated: true)
    }
    
    @IBAction func scanAgain(_ sender: UIButton) {
        if let window = (UIApplication.shared.delegate as? AppDelegate)?.window {
            window.rootViewController = storyboard?.instantiateViewController(identifier: "main")
        }

    }
    

}
