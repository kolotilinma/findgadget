//
//  MainViewController.swift
//  AirPods
//
//  Created by Михаил Колотилин on 27.03.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func startScaning(_ sender: UIButton) {
        if let window = (UIApplication.shared.delegate as? AppDelegate)?.window {
            window.rootViewController = storyboard?.instantiateViewController(identifier: "scan")
        }
    }
    

}

