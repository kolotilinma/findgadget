//
//  DevViewController.swift
//  AirPods
//
//  Created by Михаил Колотилин on 06.04.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit

class DevViewController: UIViewController {

    @IBOutlet weak var deviceNameLabel: UILabel!
    @IBOutlet weak var gaugeView: OPLGaugeView!
    @IBOutlet weak var coldLabel: UILabel!
    @IBOutlet weak var hotLabel: UILabel!
    
    var name: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        deviceNameLabel.text = name
        hotLabel.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 2)
        coldLabel.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
        NotificationCenter.default.addObserver(forName: NSNotification.Name("signal"), object: nil, queue: nil) { (notification) in
            if let value = (notification.object as? [String: CGFloat])?[self.name!] {
                self.gaugeView.rotateGauge(to: value / 100)
            }
        }
    }
    
    @IBAction func scanAgain(_ sender: UIButton) {
        if let window = (UIApplication.shared.delegate as? AppDelegate)?.window {
            window.rootViewController = storyboard?.instantiateViewController(identifier: "main")
        }
    }
    
}
