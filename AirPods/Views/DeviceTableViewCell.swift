//
//  DeviceTableViewCell.swift
//  AirPods
//
//  Created by Михаил Колотилин on 28.03.2020.
//  Copyright © 2020 Mikhail Kolitilin. All rights reserved.
//

import UIKit

class DeviceTableViewCell: UITableViewCell {

    @IBOutlet weak var deviceNameLabel: UILabel!
    @IBOutlet weak var signalLabel: UILabel!
    
    func configure(name: String) {
        deviceNameLabel.text = name
        NotificationCenter.default.addObserver(forName: NSNotification.Name("signal"), object: nil, queue: nil) { (notification) in
            if let value = (notification.object as? [String: CGFloat])?[name] {
                self.signalLabel.text = "  \(String(format: "%.f", value))%  "
            }
        }
    }

}
